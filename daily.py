import json
import urllib.request

from constants import LOGIC, META

DAILY_URL = "https://alttpr.com/en/daily"
DAILY_HASH_URL_PREFIX = "https://alttpr.com/en/h/"
HASH_URL_PREFIX = "https://alttpr.com/hash/"
HASH_STRING = "\" hash=\""
SPOILER = 'spoiler'
NAME = 'name'
PERMALINK = 'permalink'
ROM_MODE = 'rom_mode'


def get_daily_hash():
    with urllib.request.urlopen(DAILY_URL) as f:
        response = f.read().decode('UTF-8')
        hash_start = response.find(HASH_STRING) + len(HASH_STRING)
        hash_end = response.find("\"", hash_start)
        seed_hash = response[hash_start:hash_end]
    return seed_hash


def get_seed_settings(seed_hash):
    spoiler = get_spoiler(seed_hash)
    settings = spoiler[META]
    settings[LOGIC] = settings[ROM_MODE]
    settings[PERMALINK] = DAILY_HASH_URL_PREFIX + seed_hash
    return settings


def get_spoiler(seed_hash):
    with urllib.request.urlopen(HASH_URL_PREFIX + seed_hash) as f:
        response = f.read().decode('UTF-8')
        return json.loads(response)[SPOILER]
