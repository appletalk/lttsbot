# import all necessary commands and libraries

import discord
import pyz3r
import asyncio
import simplejson as json
from datetime import datetime
from discord.ext import tasks
from discord.ext.commands import Bot

from arguments import Arguments
from constants import *
from daily import get_daily_hash, get_seed_settings, PERMALINK, NAME, get_spoiler
from difficulty_ratings import get_difficulty, MAX_NORMALIZED_DIFFICULTY, DIFFICULTY_MAX_ATTEMPTS
from hints import find_in_playthough, get_hints
from image_hash import make_image_hash
from options import validate_settings, random_settings
from send_help import HINT_BRIEF, HINT_DESCRIPTION, SEED_DESCRIPTION, SEED_BRIEF, DAILY_BRIEF

with open('botconfig.json') as bot_config:
    bot_config = json.load(bot_config)

# Define discord client
client = Bot(command_prefix="!")

current_day = datetime.now().day
autopost_channels = bot_config['autopost-channels']
daily_hash = get_daily_hash()


def get_settings(minimum_difficulty, arguments):
    manual_settings = arguments.er_options if arguments.entrance_randomizer else arguments.game_options
    settings = {}
    randomize_settings = True
    attempt_count = 0
    while randomize_settings:
        attempt_count += 1
        settings = random_settings(manual_settings, arguments)
        seed_difficulty = get_difficulty(settings, arguments.entrance_randomizer)
        randomize_settings = seed_difficulty < minimum_difficulty and attempt_count < DIFFICULTY_MAX_ATTEMPTS
    return settings


def generate_seed(settings, arguments):
    gen_seed = True
    lttp_seed = None
    if arguments.seed_hash is not "":
        lttp_seed = pyz3r.alttpr(
            hash=arguments.seed_hash
        )
    else:
        randomizer_type = ENTRANCE if arguments.entrance_randomizer else ITEM
        while gen_seed:
            lttp_seed = pyz3r.alttpr(
                randomizer=randomizer_type,  # optional, defaults to item
                settings=settings
            )
            gen_seed = arguments.no_aga and find_in_playthough(lttp_seed.data[SPOILER], DEFEAT_AGAHNIM)
    return lttp_seed


@client.event
async def on_ready():
    print('Logged in as {}'.format(client.user.name))
    print('--------')


def get_settings_string(settings, seed_hints=None, seed_code="", hash_image="", is_entrance=False):
    messages = ["**{}**".format(settings[NAME])] if NAME in settings else []
    messages.extend([
        "<" + settings[PERMALINK] + ">",
        "[ Difficulty: " + str(get_difficulty(settings, is_entrance)) + " / " + str(MAX_NORMALIZED_DIFFICULTY) + " ]",
        "[ {} | {} | {} | {} | {} | {} ]".format(settings[DIFFICULTY], settings[LOGIC], settings[MODE],
                                                 settings[GOAL], settings[VARIATION],
                                                 settings[SHUFFLE] if is_entrance else settings[WEAPONS]),
    ])
    if settings.get(ENEMIZER, False):
        enemizer = settings[ENEMIZER]
        messages.append(
            "[ {} | {} | {} | {} | {} | {} ]".format(enemizer[ENEMY], enemizer[BOSSES], enemizer[ENEMY_DAMAGE],
                                                     enemizer[ENEMY_HEALTH], enemizer[POT_SHUFFLE],
                                                     enemizer[PALETTE_SHUFFLE])
        )
    if seed_hints:
        messages.extend(seed_hints)
    if seed_code and not hash_image:
        messages.append("[ {hash} ]".format(hash=' | '.join(seed_code)))
    return "\n".join(messages)


async def print_seed_info(ctx, seed_hash):
    if seed_hash:
        seed_code = pyz3r.alttpr(hash=seed_hash).code()
        hash_image = make_image_hash(seed_code)
        seed_settings = get_seed_settings(seed_hash)
        if hash_image:
            await ctx.send(get_settings_string(seed_settings),
                           file=discord.File(hash_image, filename="{}.png".format(seed_hash)))
        else:
            await ctx.send(get_settings_string(seed_settings))
    else:
        await ctx.send("No seed hash found.")


@client.command(brief=DAILY_BRIEF, description=DAILY_BRIEF)
async def daily(ctx):
    seed_hash = get_daily_hash()
    await print_seed_info(ctx, seed_hash)


@client.command(brief=HINT_BRIEF, description=HINT_DESCRIPTION)
async def hint(ctx, *args):
    arguments = Arguments()
    arguments.parse_arguments(args)

    if not arguments.hint_items:
        await ctx.send("No hint item requested")
        return

    try:
        if arguments.seed_hash is not "":
            spoiler = get_spoiler(arguments.seed_hash)
            if spoiler:
                seed_hints = get_hints(arguments.hint_items, arguments.all_progressives, spoiler)
                await ctx.send("\n".join(seed_hints))
                return
    except Exception:
        pass

    await ctx.send("No spoiler found for hash: " + arguments.seed_hash)


# !seed command
@client.command(brief=SEED_BRIEF, description=SEED_DESCRIPTION)
async def seed(ctx, *args):
    arguments = Arguments()
    arguments.parse_arguments(args)
    if arguments.seed_hash is not "":
        settings = get_seed_settings(arguments.seed_hash)
    else:
        settings = get_settings(arguments.minimum_difficulty, arguments)

    invalid_settings_message = validate_settings(settings, arguments.no_aga)
    if invalid_settings_message:
        await ctx.send("Invalid settings: " + invalid_settings_message)
        return

    try:
        lttp_seed = generate_seed(settings, arguments)
    except:
        await ctx.send("Failed to generate game.")
        await print(settings)
        raise

    seed_hints = get_hints(arguments.hint_items, arguments.all_progressives, lttp_seed.data[SPOILER])

    seed_code = lttp_seed.code()
    seed_hash = lttp_seed.hash

    hash_image = make_image_hash(seed_code)

    settings[PERMALINK] = lttp_seed.url
    seed_message = get_settings_string(settings, seed_hints, seed_code, hash_image, arguments.entrance_randomizer)
    if hash_image:
        await ctx.send(seed_message, file=discord.File(hash_image, filename="{}.png".format(seed_hash)))
    else:
        await ctx.send(seed_message)
    print("Generated seed by {} at {}\n{}\n".format(ctx.author, datetime.now(), seed_message))


# Auto post the daily seed
@tasks.loop(seconds=30)
async def auto_post_daily():
    try:
        global current_day, autopost_channels, daily_hash
        await asyncio.sleep(30)
        future_day = datetime.utcnow().day
        if future_day is not current_day or not daily_hash:
            current_day = future_day
            daily_hash = get_daily_hash()
            for autopost_channel in autopost_channels:
                channel = client.get_channel(autopost_channel)
                await print_seed_info(channel, daily_hash)
        else:
            pass
    except Exception as e:
        daily_hash = None
        print(e)

auto_post_daily.start()

# Start the bot
client.run(bot_config['discordtoken'])
