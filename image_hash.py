import io

from PIL import Image

SPRITE_WIDTH = 32
SPRITE_HEIGHT = 32
NUM_SPRITES = 5

# for reference, from pyz3r
code_map = {
    0: 'Bow', 1: 'Boomerang', 2: 'Hookshot', 3: 'Bombs',
    4: 'Mushroom', 5: 'Magic Powder', 6: 'Ice Rod', 7: 'Pendant',
    8: 'Bombos', 9: 'Ether', 10: 'Quake', 11: 'Lamp',
    12: 'Hammer', 13: 'Shovel', 14: 'Flute', 15: 'Bugnet', 16: 'Book',
    17: 'Empty Bottle', 18: 'Green Potion', 19: 'Somaria', 20: 'Cape',
    21: 'Mirror', 22: 'Boots', 23: 'Gloves', 24: 'Flippers',
    25: 'Moon Pearl', 26: 'Shield', 27: 'Tunic', 28: 'Heart',
    29: 'Map', 30: 'Compass', 31: 'Big Key'
}


def file_name(image_name):
    return 'item_sprites/' + image_name + '.png'


def get_position(image, horizontal_offset):
    width = image.size[0]
    height = image.size[1]
    x = int((SPRITE_WIDTH - width) / 2) + horizontal_offset
    y = int((SPRITE_HEIGHT - height) / 2)
    return x, y


def make_image_hash(seed_code):
    hash_images = []
    for seed_code_item in seed_code:
        try:
            hash_images.append(Image.open(file_name(seed_code_item.replace(" ", ""))))
        except:
            print("Image not found for seed code: {}".format(seed_code_item))
            hash_images = []
            break

    if hash_images:
        blank_image = Image.new("RGBA", (SPRITE_WIDTH * NUM_SPRITES, SPRITE_HEIGHT))
        for i in range(NUM_SPRITES):
            blank_image.paste(hash_images[i], get_position(hash_images[i], i * SPRITE_WIDTH))

        image_byte_array = io.BytesIO()
        blank_image.save(image_byte_array, "PNG")
        image_byte_array.seek(0)
        return image_byte_array

    return ""
