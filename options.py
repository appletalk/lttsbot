import random

from constants import *

option_difficulty = [NORMAL, HARD]
option_logic = [NO_GLITCHES]
option_mode = [STANDARD, OPEN, INVERTED]
option_variation = [NO_VARIATION, KEY_SANITY]
option_weapons = [RANDOMIZED, UNCLE, SWORDLESS]
option_goal = [GANON]
option_shuffle = [ENTRANCE_SIMPLE, RESTRICTED, FULL]

option_enemizer = [True, False]
option_spoilers = [True, False]
option_tournament = [True, False]

option_difficulty_all = [EASY, NORMAL, HARD, EXPERT, INSANE, CROWD_CONTROL]
option_logic_all = [NO_GLITCHES, OVERWORLD_GLITCHES, MAJOR_GLITCHES, NO_LOGIC]
option_mode_all = [STANDARD, OPEN, INVERTED]
option_variation_all = [NO_VARIATION, KEY_SANITY, RETRO, TIMED_RACE, TIMED_OHKO, OHKO]
option_weapons_all = [RANDOMIZED, UNCLE, SWORDLESS]
option_goal_all = [GANON, DUNGEONS, PEDESTAL, TRIFORCE_HUNT]

ER_SHUFFLE_ALL = [ENTRANCE_SIMPLE, RESTRICTED, FULL, CROSSED, INSANITY]
ER_GOALS_ALL = option_goal_all + [CRYSTALS]
ER_MODE_ALL = [OPEN, SWORDLESS]
ER_LOGIC_ALL = [NO_GLITCHES]

ALL_OPTIONS = {
    DIFFICULTY: option_difficulty_all,
    LOGIC: option_logic_all,
    MODE: option_mode_all,
    VARIATION: option_variation_all,
    WEAPONS: option_weapons_all,
    GOAL: option_goal_all
}

ER_ALL_OPTIONS = {
    DIFFICULTY: option_difficulty_all,
    LOGIC: ER_LOGIC_ALL,
    MODE: ER_MODE_ALL,
    VARIATION: option_variation_all,
    GOAL: ER_GOALS_ALL,
    SHUFFLE: ER_SHUFFLE_ALL
}

OPTION_ENEMY_ALL = [True, False]
OPTION_BOSSES_ALL = [BOSS_OFF, BOSS_SIMPLE, BOSS_FULL, BOSS_CHAOS]
OPTION_DAMAGE_ALL = [DAMAGE_DEFAULT, DAMAGE_SHUFFLED, DAMAGE_CHAOS]
OPTION_HEALTH_ALL = list(range(0, 4))
OPTION_POT_ALL = [True, False]
OPTION_PALETTE_ALL = [True, False]

ALL_ENEMIZER_OPTIONS = {
    ENEMY: OPTION_ENEMY_ALL,
    BOSSES: OPTION_BOSSES_ALL,
    ENEMY_DAMAGE: OPTION_DAMAGE_ALL,
    ENEMY_HEALTH: OPTION_HEALTH_ALL,
    POT_SHUFFLE: OPTION_POT_ALL,
    PALETTE_SHUFFLE: OPTION_PALETTE_ALL
}

CROSSKEYS_OPTIONS = {
    DIFFICULTY: NORMAL,
    LOGIC: NO_GLITCHES,
    MODE: OPEN,
    VARIATION: KEY_SANITY,
    GOAL: CRYSTALS,
    SHUFFLE: CROSSED
}


def random_settings(manual_settings, arguments):
    possible_goals = [goal for goal in option_goal if goal != DUNGEONS] if arguments.no_aga else option_goal
    return {
        DIFFICULTY: manual_settings.get(DIFFICULTY, random.choice(option_difficulty)),
        ENEMIZER: random_enemizer_settings() if manual_settings.get(ENEMIZER) else False,
        LOGIC: manual_settings.get(LOGIC, random.choice(option_logic)),
        MODE: manual_settings.get(MODE, random.choice(option_mode)),
        GOAL: manual_settings.get(GOAL, random.choice(possible_goals)),
        SPOILERS: arguments.spoilers,  # haven't figured out how to make this actually work
        TOURNAMENT: arguments.tournament,
        VARIATION: manual_settings.get(VARIATION, random.choice(option_variation)),
        WEAPONS: manual_settings.get(WEAPONS, random.choice(option_weapons)),
        SHUFFLE: manual_settings.get(SHUFFLE, random.choice(option_shuffle)) if arguments.entrance_randomizer else None,
        LANGUAGE: "en"
    }


def random_enemizer_settings():
    enemizer_settings = {}
    for option in ALL_ENEMIZER_OPTIONS.keys():
        enemizer_settings[option] = random.choice(ALL_ENEMIZER_OPTIONS[option])
    return enemizer_settings


def get_manual_settings(args, option_dict):
    manual_settings = {}
    for arg in args:
        if arg.upper() == NO_VARIATION.upper():
            manual_settings[VARIATION] = NO_VARIATION
            continue
        if arg.upper() == NO_LOGIC_ARGUMENT.upper():
            manual_settings[LOGIC] = NO_LOGIC
            continue
        if arg.upper() == ENEMIZER.upper():
            manual_settings[ENEMIZER] = True
        for option in option_dict.keys():
            for option_name in option_dict[option]:
                if arg.upper() == option_name.upper():
                    manual_settings[option] = option_name
    return manual_settings


def validate_settings(settings, is_no_aga):
    return "" if not (settings[GOAL] == DUNGEONS and is_no_aga) else "Aga required in all dungeons seeds"
