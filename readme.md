# lttsbot

A discord bot for generating ALttPR seeds.

Utilizes pyz3r by tcprescott: https://github.com/tcprescott/pyz3r

## Bot config

Edit botconfig.json to hook the bot up to a discord server.

`discordtoken`: token of the discord server
`autopost-channels`: array of channel id's to auto post the daily in

## Commands

### !daily

Get the current daily seed. The daily can also be auto posted in a channel when it is released every day.

### !hint

Get the locations of items in a seed

##### Usage:

```
!hint <seed hash> [item names] [All]
```

##### Example

```
!hint 35MzYzKgGE boots glove
```

All: using this option will list all progressive locations for swords etc. otherwise a random location is chosen

Valid item names: PegasusBoots, OcarinaInactive, Boomerang, RedBoomerang, Mushroom, Hookshot, Flippers, Powder, ProgressiveGlove, SilverArrowUpgrade, Hammer, Bombos, Ether, Lamp, ProgressiveSword, MoonPearl, BookOfMudora, FireRod, IceRod, CaneOfByrna, CaneOfSomaria, Bow, Quake, boots, flute, bloomerang, glove, silvers, sword, pearl, book, bluecane, redcane, net, mail


##### !seed

Generate a seed

### Usage

```
!seed [game options] [hint item names] [difficulty score] [NoAga] [All] [Tournament] [Crosskeys] [Entrance] [seed hash]
```

### Examples

```
!seed
!seed 3.4
!seed normal randomized ganon key-sanity overworldglitches boots hookshot noaga
!seed crosskeys
!seed entrance insanity  seed sword glove all  seed 35MzYzKgGE
```

### Options

All: progressive hints will show all locations instead of a randomized one

Crosskeys: crossed world keysanity crystals preset

Entrance: entrance randomizer seed, game options slightly different

NoAga: aga tower not required

Tournament: race roms don’t have spoiler logs

Difficulty score: minimum difficulty score

Hint item names: item names, see hint command

Seed hash: enter this to retrieve the seed instead of generating

##### Game options

Difficulty: easy, normal, hard, expert, insane, crowdControl

Goal: ganon, dungeons, pedestal, triforce-hunt

Logic: NoGlitches, OverworldGlitches, MajorGlitches, None

Mode: standard, open, inverted

Variation: none, key-sanity, retro, timed-race, timed-ohko, ohko

Weapons: randomized, uncle, swordless

