import re

def is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def is_seed_hash(value):
    # Maybe possible to generate a seed that doesn't match this regex.
    #if re.match("^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]){10}", value):
    if re.match("^(?=.*[a-z])(?=.*[A-Z]){10}", value):
        return True
    else:
        return False
