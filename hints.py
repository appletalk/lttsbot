import random

from constants import PLAYTHROUGH, RACE_ROMS, META, TOURNAMENT
from items import items, item_alt_names, PROGRESSIVE


def get_requested_hints(args):
    requested_hints = []
    for arg in args:
        for item in items:
            if arg.upper() == item.upper():
                requested_hints.append(item)
        for key in item_alt_names.keys():
            if item_alt_names[key].upper() == arg.upper():
                requested_hints.append(key)
    return requested_hints


def get_item_locations(spoiler_areas, item_name):
    locations = []
    is_progressive = PROGRESSIVE.upper() in item_name.upper()
    for area in spoiler_areas:
        for location in spoiler_areas[area]:
            try:
                item = spoiler_areas[area][location]
                if item.upper().replace(" ", "") == item_name.upper():
                    locations.append(location)
                    if not is_progressive:
                        return locations
            except Exception as e:
                pass
    return locations


def find_in_playthough(spoiler, to_find):
    found_details = {}

    for sphere in spoiler[PLAYTHROUGH]:
        if sphere in ["regions_visited", "longest_item_chain"]:
            continue
        locations = get_item_locations(spoiler[PLAYTHROUGH][sphere], to_find)
        if locations:
            found_details["location"] = locations[0]
            found_details["sphere"] = sphere
            return found_details


def format_seed_hint(item, location):
    return "[ {} location: {} ]".format(item, location)


def get_hints(requested_hints, all_progressives, spoiler):
    seed_hints = []
    if spoiler.get(META, {}).get(TOURNAMENT, False):
        seed_hints.append("[ CHEATER! " + RACE_ROMS + " ]")
        return seed_hints
    for item_name in requested_hints:
        item_locations = get_item_locations(spoiler, item_name)
        if not item_locations:
            item_locations = [None]

        if all_progressives:
            for item_location in item_locations:
                seed_hints.append(format_seed_hint(item_name, item_location))
        else:
            item_location = random.choice(item_locations)
            seed_hints.append(format_seed_hint(item_name, item_location))
    return seed_hints
