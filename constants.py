EASY = "easy"
NORMAL = "normal"
HARD = "hard"
EXPERT = "expert"
INSANE = "insane"
CROWD_CONTROL = "crowdControl"

NO_GLITCHES = "NoGlitches"
OVERWORLD_GLITCHES = "OverworldGlitches"
MAJOR_GLITCHES = "MajorGlitches"
NO_LOGIC = "None"

NO_LOGIC_ARGUMENT = "NO-LOGIC"

STANDARD = "standard"
OPEN = "open"
INVERTED = "inverted"

GANON = "ganon"
DUNGEONS = "dungeons"
PEDESTAL = "pedestal"
TRIFORCE_HUNT = "triforce-hunt"
CRYSTALS = "crystals"

NO_VARIATION = "none"
KEY_SANITY = "key-sanity"
RETRO = "retro"
TIMED_RACE = "timed-race"
TIMED_OHKO = "timed-ohko"
OHKO = "ohko"

RANDOMIZED = "randomized"
UNCLE = "uncle"
SWORDLESS = "swordless"

ENTRANCE_SIMPLE = "simple"
RESTRICTED = "restricted"
FULL = "full"
CROSSED = "crossed"
INSANITY = "insanity"

CROSSKEYS = "crosskeys"

# for some reason these are different on the site and in the code
BOSS_OFF = "off"
BOSS_SIMPLE = "basic"
BOSS_FULL = "normal"
BOSS_CHAOS = "chaos"

DAMAGE_DEFAULT = "default"
DAMAGE_SHUFFLED = "shuffled"
DAMAGE_CHAOS = "chaos"


LANGUAGE = "lang"
WEAPONS = "weapons"
VARIATION = "variation"
TOURNAMENT = "tournament"
SPOILERS = "spoilers"
MODE = "mode"
GOAL = "goal"
LOGIC = "logic"
ENEMIZER = "enemizer"
DIFFICULTY = "difficulty"
SHUFFLE = "shuffle"

ENEMY = "enemy"
BOSSES = "bosses"
ENEMY_DAMAGE = "enemy_damage"
ENEMY_HEALTH = "enemy_health"
POT_SHUFFLE = "pot_shuffle"
PALETTE_SHUFFLE = "palette_shuffle"

ENTRANCE = "entrance"
ITEM = "item"

PLAYTHROUGH = "playthrough"
DEFEAT_AGAHNIM = "DefeatAgahnim"

HASH = "hash"
SPOILER = "spoiler"

ALL = "all"

META = "meta"

RACE_ROMS = "race roms don’t have spoiler logs"
