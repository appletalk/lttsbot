import pprint

from constants import RACE_ROMS
from items import items, item_alt_names
from options import ALL_OPTIONS

GAME_OPTIONS = "game options"
HINTS = "hint item names"
SCORE = "difficulty score"
NO_AGA = "NoAga"
ALL = "All"
TOURNAMENT = "Tournament"
CROSSKEYS = "Crosskeys"
ENTRANCE = "Entrance"
SEED_HASH = "seed hash"

SEED_OPTIONS = {
    GAME_OPTIONS: ALL_OPTIONS,
    HINTS: "item names, see hint command help",
    SCORE: "minimum difficulty score",
    NO_AGA: "aga tower not required",
    ALL: "progressive hints will show all locations instead of a randomized one",
    TOURNAMENT: RACE_ROMS,
    CROSSKEYS: "crossed world keysanity crystals preset",
    ENTRANCE: "entrance randomizer seed, game options slightly different",
    SEED_HASH: "enter this to retrieve the seed instead of generating"
}

DAILY_BRIEF = "Get the current daily seed"
HINT_BRIEF = "Get the locations of items in a seed"
SEED_BRIEF = "Generate a seed"

HINT_DESCRIPTION = '\n'.join(
    [HINT_BRIEF,
     "",
     "Usage:",
     "  hint <seed hash> [item names] [All]",
     "",
     "Ex:",
     "  hint 35MzYzKgGE boots glove",
     "",
     "All: using this option will list all progressive locations for swords etc. otherwise a random location is chosen",
     "",
     "Valid item names: " + ', '.join(items + list(item_alt_names.values()))])

SEED_DESCRIPTION = '\n'.join(
    [SEED_BRIEF,
     "",
     "Usage:",
     "  seed [" + "] [".join(SEED_OPTIONS) + "]",
     "",
     "Examples:",
     "  seed normal randomized ganon key-sanity overworldglitches boots hookshot noaga",
     "  seed crosskeys",
     "  seed entrance insanity"
     "  seed sword glove all"
     "  seed 35MzYzKgGE",
     "",
     "Options: ",
     pprint.PrettyPrinter(indent=0, width=120).pformat(SEED_OPTIONS)
         .replace("{", "").replace("}", "").replace("'", "").replace('"', '').replace("[", "").replace("],", "").replace(",\n", "\n"),
     ])
