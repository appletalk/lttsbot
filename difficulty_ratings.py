from constants import *
from options import option_logic, option_mode, option_variation, option_weapons, option_difficulty

difficulty = {
    EASY: 0,
    NORMAL: 2,
    HARD: 5,
    EXPERT: 8,
    INSANE: 13
}

logic = {
    NO_GLITCHES: 0,
    OVERWORLD_GLITCHES: 3,
    MAJOR_GLITCHES: 8,
    NO_LOGIC: 13
}

mode = {
    STANDARD: 0,
    OPEN: 1,
    INVERTED: 5,
    SWORDLESS: 5,
}

variation = {
    NO_VARIATION: 0,
    KEY_SANITY: 3,
    RETRO: 3,
    TIMED_RACE: 0,
    TIMED_OHKO: 8,  # weird mode not rating it
    OHKO: 13
}

weapons = {
    UNCLE: 0,
    RANDOMIZED: 1,
    SWORDLESS: 5
}

goals = {
    TRIFORCE_HUNT: 0,
    PEDESTAL: 2,
    GANON: 3,
    CRYSTALS: 3,
    DUNGEONS: 5
}

SHUFFLES = {
    ENTRANCE_SIMPLE: 2,
    RESTRICTED: 3,
    FULL: 5,
    CROSSED: 8,
    INSANITY: 13
}


def max_option_value(options, difficulties):
    max_difficulty = 0
    for option in options:
        max_difficulty = max(max_difficulty, difficulties.get(option, 0))
    return max_difficulty


MIN_NORMALIZE_DIFFICULTY = 1
MAX_NORMALIZED_DIFFICULTY = 5
MAX_DIFFICULTY = max_option_value(option_difficulty, difficulty) + \
                 max_option_value(option_logic, logic) + \
                 max_option_value(option_mode, mode) + \
                 max_option_value(option_variation, variation) + \
                 max_option_value(option_weapons, weapons)
DIFFICULTY_MAX_ATTEMPTS = 100


def get_difficulty(settings, is_entrance):
    difficult_score = difficulty[settings[DIFFICULTY]] + \
                      logic[settings[LOGIC]] + \
                      mode[settings[MODE]] + \
                      variation[settings[VARIATION]] + \
                      (SHUFFLES[settings[SHUFFLE]] if is_entrance else weapons[settings[WEAPONS]])

    return round(difficult_score / MAX_DIFFICULTY * (
                MAX_NORMALIZED_DIFFICULTY - MIN_NORMALIZE_DIFFICULTY), 1) + MIN_NORMALIZE_DIFFICULTY
