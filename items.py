PEGASUS_BOOTS = "PegasusBoots"
OCARINA_INACTIVE = "OcarinaInactive"
BLUE_BOOMERANG = "Boomerang"
RED_BOOMERANG = "RedBoomerang"
MUSHROOM = "Mushroom"
HOOKSHOT = "Hookshot"
FLIPPERS = "Flippers"
POWDER = "Powder"
GLOVE = "ProgressiveGlove"
SILVERS = "SilverArrowUpgrade"
HAMMER = "Hammer"
BOMBOS = "Bombos"
ETHER = "Ether"
LAMP = "Lamp"
SWORD = "ProgressiveSword"
MOON_PEARL = "MoonPearl"
BOOK = "BookOfMudora"
FIRE_ROD = "FireRod"
ICE_ROD = "IceRod"
BLUE_CANE = "CaneOfByrna"
RED_CANE = "CaneOfSomaria"
BOW = "Bow"
QUAKE = "Quake"
NET = "BugCatchingNet"
ARMOR = "ProgressiveArmor"

PROGRESSIVE = "Progressive"

item_alt_names = {
    PEGASUS_BOOTS: "boots",
    OCARINA_INACTIVE: "flute",
    BLUE_BOOMERANG: "bloomerang",
    GLOVE: "glove",
    SILVERS: "silvers",
    SWORD: "sword",
    MOON_PEARL: "pearl",
    BOOK: "book",
    BLUE_CANE: "bluecane",
    RED_CANE: "redcane",
    NET: "net",
    ARMOR: "mail"
}

items = [
    PEGASUS_BOOTS,
    OCARINA_INACTIVE,
    BLUE_BOOMERANG,
    RED_BOOMERANG,
    MUSHROOM,
    HOOKSHOT,
    FLIPPERS,
    POWDER,
    GLOVE,
    SILVERS,
    HAMMER,
    BOMBOS,
    ETHER,
    LAMP,
    SWORD,
    MOON_PEARL,
    BOOK,
    FIRE_ROD,
    ICE_ROD,
    BLUE_CANE,
    RED_CANE,
    BOW,
    QUAKE,
]