from constants import ALL, ENTRANCE, CROSSKEYS, SPOILER, TOURNAMENT
from difficulty_ratings import MAX_NORMALIZED_DIFFICULTY, MIN_NORMALIZE_DIFFICULTY
from hints import get_requested_hints
from options import get_manual_settings, ALL_OPTIONS, ER_ALL_OPTIONS, CROSSKEYS_OPTIONS
from utils import is_float, is_seed_hash


class Arguments:
    NOAGA = 'noaga'

    def __init__(self):
        self.hint_items = []
        self.game_options = {}
        self.er_options = {}
        self.seed_hash = ""
        self.no_aga = False
        self.all_progressives = False
        self.minimum_difficulty = MIN_NORMALIZE_DIFFICULTY
        self.entrance_randomizer = False
        self.skip_manual_settings = False
        self.spoilers = False
        self.tournament = False

    def parse_arguments(self, args):
        for arg in args:
            if is_seed_hash(arg):
                self.seed_hash = arg
                continue

            if is_float(arg):
                self.minimum_difficulty = min(float(arg), MAX_NORMALIZED_DIFFICULTY)
                continue

            if arg.upper() == Arguments.NOAGA.upper():
                self.no_aga = True
                continue

            if arg.upper() == ALL.upper():
                self.all_progressives = True
                continue

            if arg.upper() == SPOILER.upper():
                self.spoilers = True
                continue

            if arg.upper() == TOURNAMENT.upper():
                self.tournament = True
                continue

            if arg.upper() == CROSSKEYS.upper():
                self.er_options = CROSSKEYS_OPTIONS
                self.entrance_randomizer = True
                self.skip_manual_settings = True

            if not self.skip_manual_settings:
                if arg.upper() == ENTRANCE.upper():
                    self.entrance_randomizer = True

                game_option = get_manual_settings([arg], ALL_OPTIONS)
                if game_option:
                    self.game_options.update(game_option)

                # probably would be better to look through all args for entrance option first
                er_option = get_manual_settings([arg], ER_ALL_OPTIONS)
                if er_option:
                    self.er_options.update(er_option)
                    continue

                if game_option:
                    continue

            hint_item = get_requested_hints([arg])
            if hint_item:
                self.hint_items.extend(hint_item)
